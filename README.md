# Why3 CLI Rust bindings

[Why3](http://why3.lri.fr/) is a platform for deductive program verification.
The Why3 distribution comes with a CLI tool and an OCaml API. Since binding
OCaml code to Rust is not a very explored territory, this crate provides a Rust
API for the Why3 CLI instead.

## Dependencies

The `why3` executable (or an executable with identical interface) needs to be
installed and marked as executable.

Additionally prover backends can be installed.

For running the automated tests, the `why3` executable needs to be accessible
from the `PATH` environment variable, as well as `z3` is expected to be
installed.

To run the tests, run `cargo test`.
