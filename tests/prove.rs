/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::io::Write;
use why3::{command::prove, Why3};

#[test]
fn prove_simple() {
    let why = Why3::new();

    let mut file = tempfile::Builder::new().suffix(".why").tempfile().unwrap();

    writeln!(
        file,
        r#"
        theory Test
            goal AlwaysTrue: true
        end
        "#
    )
    .unwrap();

    let res = why.prove("z3-ce").file(file.path()).execute().unwrap();

    assert_eq!(res.len(), 1);
    assert_eq!(&res[0].theory, "Test");
    assert_eq!(&res[0].goal, "AlwaysTrue");
    assert_eq!(res[0].status, prove::ProveStatus::Valid);
}

#[test]
fn prove_simple_with_theory() {
    let why = Why3::new();

    let mut file = tempfile::Builder::new().suffix(".why").tempfile().unwrap();

    writeln!(
        file,
        r#"
        theory Test
            goal AlwaysTrue: true
        end
        "#
    )
    .unwrap();

    let theory = prove::TheoryDesc::new("Test");

    let res = why
        .prove("z3-ce")
        .file_with_theory(file.path(), theory)
        .execute()
        .unwrap();

    assert_eq!(res.len(), 1);
    assert_eq!(&res[0].theory, "Test");
    assert_eq!(&res[0].goal, "AlwaysTrue");
    assert_eq!(res[0].status, prove::ProveStatus::Valid);
}

#[test]
fn prove_simple_with_theory_and_goal() {
    let why = Why3::new();

    let mut file = tempfile::Builder::new().suffix(".why").tempfile().unwrap();

    writeln!(
        file,
        r#"
        theory Test
            goal AlwaysTrue: true
        end
        "#
    )
    .unwrap();

    let theory = prove::TheoryDesc::new("Test").goal("AlwaysTrue");

    let res = why
        .prove("z3-ce")
        .file_with_theory(file.path(), theory)
        .execute()
        .unwrap();

    assert_eq!(res.len(), 1);
    assert_eq!(&res[0].theory, "Test");
    assert_eq!(&res[0].goal, "AlwaysTrue");
    assert_eq!(res[0].status, prove::ProveStatus::Valid);
}

#[test]
fn prove_simple_with_theory_and_multiple_goals() {
    let why = Why3::new();

    let mut file = tempfile::Builder::new().suffix(".why").tempfile().unwrap();

    writeln!(
        file,
        r#"
        theory Test
            goal Goal1: true

            goal Goal2: not false
        end
        "#
    )
    .unwrap();

    let theory = prove::TheoryDesc::new("Test").goals(&["Goal1", "Goal2"]);

    let res = why
        .prove("z3-ce")
        .file_with_theory(file.path(), theory)
        .execute()
        .unwrap();

    assert_eq!(res.len(), 2);
    assert_eq!(&res[0].theory, "Test");
    assert_eq!(&res[0].goal, "Goal1");
    assert_eq!(res[0].status, prove::ProveStatus::Valid);

    assert_eq!(&res[1].theory, "Test");
    assert_eq!(&res[1].goal, "Goal2");
    assert_eq!(res[1].status, prove::ProveStatus::Valid);
}

#[test]
fn prove_max_sum() {
    let why = Why3::new();

    let mut file = tempfile::Builder::new().suffix(".why").tempfile().unwrap();

    writeln!(
        file,
        r#"
        module MaxAndSum

            use int.Int
            use ref.Ref
            use array.Array

            let max_sum (a: array int) (n: int) : (int, int)
                requires {{ n = length a }}
                requires {{ forall i. 0 <= i < n -> a[i] >= 0 }}
                returns {{ sum, max -> sum <= n * max }}
            =
                let sum = ref 0 in
                let max = ref 0 in

                for i = 0 to n - 1 do
                    invariant {{ !sum <= i * !max }}

                    if !max < a[i] then
                        max := a[i];

                    sum := !sum + a[i];
                done;

                !sum, !max

        end
        "#
    )
    .unwrap();

    let res = why.prove("z3-ce").file(file.path()).execute().unwrap();

    assert_eq!(res.len(), 1);
    assert_eq!(&res[0].theory, "MaxAndSum");
    assert_eq!(&res[0].goal, "max_sum");
    assert_eq!(res[0].status, prove::ProveStatus::Valid);
}
