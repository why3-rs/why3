/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use why3::Why3;

#[test]
fn version_matches() {
    let why3 = Why3::new();

    let version = why3.version().unwrap();

    assert_eq!(&version, "1.2.0");
}
