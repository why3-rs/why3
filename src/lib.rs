/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! Why3 formal verification framework.
//!
//! The Why3 toolkit can be used to formally verify computer algorithms and
//! programs.
//!
//! This crate provides a Rust API to the Why3 CLI since the native APIs are
//! written in OCaml and creating bindings is a lot harder than wrapping a CLI.

#![forbid(unsafe_code)]
#![warn(missing_docs)]

pub mod builder;
pub mod command;

/// Name of a prover backend.
pub type ProverName = String;
/// Name of a theory or module.
pub type TheoryName = String;
/// Name of a goal (or other target to prove).
pub type GoalName = String;

/// Connection to the Why3 executable.
#[derive(Debug)]
pub struct Why3 {
    pub(crate) builder: builder::Builder,
}

impl Default for Why3 {
    fn default() -> Self {
        Self::new()
    }
}

impl Why3 {
    /// Create a new setup to call the Why3 CLI using default values.
    ///
    /// The CLI should be named `why3` and be accessible through the `PATH`
    /// environment variable.
    ///
    /// If a custom path needs to be supplied, use the [`builder`] method
    /// instead.
    ///
    /// [`builder`]: struct.Why3.html#method.builder
    pub fn new() -> Self {
        Self::builder().build()
    }

    /// Setup the Why3 connection using a builder object.
    pub fn builder() -> builder::Builder {
        builder::Builder::new()
    }

    /// Returns the version of the installed Why3 toolchain.
    pub fn version(&self) -> Result<String, Error> {
        let mut command = self.builder.build_command();
        command.arg("--version");

        let output = execute(command)?;

        let version_str = output
            .stdout
            .trim_start_matches("Why3 platform, version ")
            .trim_end_matches('\n');

        Ok(version_str.into())
    }

    /// Invoke a "prove" command.
    ///
    /// Returns a builder for the prove command.
    pub fn prove<P>(
        &self,
        prover: P,
    ) -> command::prove::Prove<'_, command::prove::FileUnknown>
    where
        P: Into<ProverName>,
    {
        command::prove::Prove::new(self, prover)
    }

    pub(crate) fn command(&self) -> std::process::Command {
        self.builder.build_command()
    }
}

/// The error type for Why3 operations and output parsing.
#[derive(Debug)]
pub enum Error {
    /// The Why3 executable could not be executed.
    FailedToExecute(std::io::Error),

    /// A call to Why3 yielded output on `stderr`.
    ExecutionError {
        /// The output written to `stderr`.
        stderr: String,
    },
    /// The result of a prove command yielded an error.
    ProveError(command::prove::Error),
}

#[derive(Debug)]
pub(crate) struct Output {
    pub(crate) stdout: String,
    pub(crate) stderr: String,
}

pub(crate) fn execute(mut cmd: std::process::Command) -> Result<Output, Error> {
    let output = cmd.output().map_err(Error::FailedToExecute)?;

    let stdout = String::from_utf8_lossy(&output.stdout).to_string();
    let stderr = String::from_utf8_lossy(&output.stderr).to_string();

    Ok(Output { stdout, stderr })
}
