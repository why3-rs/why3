/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! Prove command of `Why3`.

/// Type used for setting up a "typestate" pattern.
///
/// If this state is used then no file path has been set.
#[derive(Debug, Copy, Clone)]
pub struct FileUnknown;

/// Type used for setting up a "typestate" pattern.
///
/// If this state is used then one or more file paths have been set.
#[derive(Debug, Copy, Clone)]
pub struct FileProvided;

mod sealed {
    pub trait FileStateImpl {}

    impl FileStateImpl for super::FileUnknown {}
    impl FileStateImpl for super::FileProvided {}
}

/// Common trait for type-states used in the [`Prove`] builder/structure.
///
/// [`Prove`]: struct.Prove.html
pub trait FileState: sealed::FileStateImpl + Copy {}

impl<T: sealed::FileStateImpl + Copy> FileState for T {}

/// Builder and executor of a `Why3` "prove" command.
///
/// Use [`Why3::prove()`][prv] to obtain an object of this type.
///
/// [prv]: ../../struct.Why3.html#method.prove
#[derive(Debug)]
pub struct Prove<'a, F: FileState> {
    why3: &'a crate::Why3,
    prover: crate::ProverName,
    files: Vec<FileOption>,
    _state: std::marker::PhantomData<F>,
}

impl<'a> Prove<'a, FileUnknown> {
    pub(crate) fn new<P>(why3: &'a crate::Why3, prover: P) -> Self
    where
        P: Into<crate::ProverName>,
    {
        let prover = prover.into();
        Prove {
            why3,
            prover,
            files: vec![],
            _state: std::marker::PhantomData,
        }
    }
}

impl<'a, F: FileState> Prove<'a, F> {
    /// Prove the file given by `path`.
    pub fn file<P>(mut self, path: P) -> Prove<'a, FileProvided>
    where
        P: AsRef<std::path::Path>,
    {
        let file_option = FileOption {
            name: path.as_ref().into(),
            theory: None,
        };
        self.files.push(file_option);
        Prove {
            why3: self.why3,
            prover: self.prover,
            files: self.files,
            _state: std::marker::PhantomData,
        }
    }

    /// Prove a theory or module in the file given by `path`.
    pub fn file_with_theory<P>(
        mut self,
        path: P,
        theory_desc: TheoryDesc,
    ) -> Prove<'a, FileProvided>
    where
        P: AsRef<std::path::Path>,
    {
        let file_option = FileOption {
            name: path.as_ref().into(),
            theory: Some(theory_desc),
        };
        self.files.push(file_option);
        Prove {
            why3: self.why3,
            prover: self.prover,
            files: self.files,
            _state: std::marker::PhantomData,
        }
    }
}

impl<'a> Prove<'a, FileProvided> {
    /// Execute the "prove" command and return the results.
    pub fn execute(self) -> Result<Vec<ProveResult>, crate::Error> {
        let mut command = self.why3.command();

        // add options to command
        {
            command.arg("prove");
            command.arg("-P");
            command.arg(&self.prover);

            for file in &self.files {
                command.arg(&file.name);
                if let Some(theory) = file.theory.as_ref() {
                    command.arg("-T");
                    command.arg(&theory.name);

                    for goal in &theory.goals {
                        command.arg("-G");
                        command.arg(goal);
                    }
                }
            }
        }

        let output = crate::execute(command)?;

        dbg!(&output);

        if output.stderr.is_empty() {
            parse_prove_output(&output.stdout).map_err(crate::Error::ProveError)
        } else {
            Err(crate::Error::ProveError(Error::ExecuteError {
                stderr: output.stderr,
            }))
        }
    }
}

/// Description of a `Theory` or `Module` and `Goals` to prove.
#[derive(Debug)]
pub struct TheoryDesc {
    name: crate::TheoryName,
    goals: Vec<crate::GoalName>,
}

impl TheoryDesc {
    /// Create a new description for the theory/module `name`.
    pub fn new<N>(name: N) -> Self
    where
        N: Into<crate::TheoryName>,
    {
        Self {
            name: name.into(),
            goals: vec![],
        }
    }

    /// Include a goal to prove.
    pub fn goal<N>(mut self, goal: N) -> Self
    where
        N: Into<crate::GoalName>,
    {
        self.goals.push(goal.into());
        self
    }

    /// Include a list of goals to prove.
    pub fn goals<I>(mut self, goals: I) -> Self
    where
        I: IntoIterator,
        I::Item: AsRef<str>,
    {
        for goal in goals.into_iter() {
            self.goals.push(goal.as_ref().into());
        }
        self
    }
}

#[derive(Debug)]
struct FileOption {
    name: std::path::PathBuf,
    theory: Option<TheoryDesc>,
}

/// Prove-result of a goal.
#[derive(Debug)]
pub struct ProveResult {
    /// File in which the goal is located.
    pub file: String,
    /// `Theory` or `Module` the goal is located in.
    pub theory: crate::TheoryName,
    /// Name of the goal.
    pub goal: crate::GoalName,
    /// Status of the prove operation.
    pub status: ProveStatus,
}

/// Status of a prove operation.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ProveStatus {
    /// The goal was proven to be valid.
    Valid,
    /// The goal's correctness could not be proven.
    Unknown,
    /// The prover timed out while proving the goal.
    Timeout,
    /// The goal could not be proven to be correct.
    Failure,
    /// The goal to prove is invalid.
    Invalid,
}

/// The error type for prove operations and output parsing.
#[derive(Debug)]
pub enum Error {
    /// The output of a prove command could not be parsed.
    ParseError,
    /// The prove command did not execute correctly and printed information on
    /// `stderr`.
    ExecuteError {
        /// The output written to `stderr`.
        stderr: String,
    },
}

pub(crate) fn parse_prove_output(
    stdout: &str,
) -> Result<Vec<ProveResult>, Error> {
    let mut results = vec![];

    for line in stdout.lines() {
        if line.is_empty() {
            continue;
        }

        results.push(parse_line(line)?);
    }

    Ok(results)
}

fn parse_line(line: &str) -> Result<ProveResult, Error> {
    let splits: Vec<_> = line.split(' ').collect();

    if splits.len() < 4 {
        return Err(Error::ParseError);
    }

    let file = splits[0];
    let theory = splits[1];

    // Sometimes the output contains a `VC` after the theory/module.
    // In order to parse the goal correctly the next item that ends with `:`
    // is selected.

    let mut i = 2;
    let goal: &str;

    loop {
        if i >= splits.len() {
            return Err(Error::ParseError);
        }
        if splits[i].ends_with(':') {
            goal = splits[i].trim_end_matches(':');
            break;
        }
        i += 1;
    }

    let result_str = splits[i + 1];

    let result = match result_str {
        "Valid" => ProveStatus::Valid,
        "Unknown" => ProveStatus::Unknown,
        "Timeout" => ProveStatus::Timeout,
        "Failure" => ProveStatus::Failure,
        "Invalid" => ProveStatus::Invalid,
        _ => return Err(Error::ParseError),
    };

    Ok(ProveResult {
        file: file.into(),
        theory: theory.into(),
        goal: goal.into(),
        status: result,
    })
}
