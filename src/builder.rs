/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! Builder for the connection to the Why3 CLI executable.

use std::path::PathBuf;

/// Builder for setting up and customizing the Why3 CLI connection.
#[derive(Debug)]
pub struct Builder {
    exec_path: PathBuf,
    config: Option<PathBuf>,
    extra_config: Option<PathBuf>,
}

impl Default for Builder {
    fn default() -> Self {
        Self::new()
    }
}

impl Builder {
    /// Create a new `Builder` object.
    pub fn new() -> Self {
        Self {
            exec_path: "why3".into(),
            config: None,
            extra_config: None,
        }
    }

    /// Set the path of the `why3` executable.
    pub fn executable_path<P>(mut self, path: P) -> Self
    where
        P: AsRef<std::path::Path>,
    {
        let path: PathBuf = path.as_ref().into();

        self.exec_path = path;

        self
    }

    /// Set the path to the config file of the `why3` executable.
    pub fn config<P>(mut self, path: P) -> Self
    where
        P: AsRef<std::path::Path>,
    {
        let path: PathBuf = path.as_ref().into();

        self.config = Some(path);

        self
    }

    /// Set the path to the extra-config file of the `why3` executable.
    pub fn extra_config<P>(mut self, path: P) -> Self
    where
        P: AsRef<std::path::Path>,
    {
        let path: PathBuf = path.as_ref().into();

        self.extra_config = Some(path);

        self
    }

    /// Finish the build-process.
    pub fn build(self) -> crate::Why3 {
        crate::Why3 { builder: self }
    }

    pub(crate) fn build_command(&self) -> std::process::Command {
        let mut command = std::process::Command::new(&self.exec_path);

        if let Some(path) = &self.config {
            command.arg("--config").arg(&path);
        }

        if let Some(path) = &self.extra_config {
            command.arg("--extra-config").arg(&path);
        }

        command
    }
}
